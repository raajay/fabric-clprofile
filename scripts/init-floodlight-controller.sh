#!/bin/bash

#install java
apt-get update
apt-get install --assume-yes ant
apt-get install --assume-yes curl
apt-get install --assume-yes default-jre
apt-get install --assume-yes default-jdk


cd /local/repository
git submodule update --init --recursive

cd /local/repository/software/floodlight
ant

mkdir -p /local/repository/logs

cd /local/repository/software/floodlight
java -jar target/floodlight.jar 2>&1 | tee /local/repository/logs/floodlight.log > /dev/null
