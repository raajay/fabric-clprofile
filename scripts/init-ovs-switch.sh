#!/bin/bash

if [[ -z $1 ]]; then
    mkdir -p /local/repository/
    echo "No parameters passed to the script." >> /local/repository/init-ovs-switch.err
else
    ovs-vsctl add-br br0

    # shut down all interfaces other than eth0 and eth1
    for i in `seq 2 $1`; do
        ifconfig eth$i 0
    done

    ovs-vsctl list-ports br0 >> /local/repository/init-ovs-switch.log

    # add the interfaces to the bridge
    for i in `seq 2 $1`; do
        ovs-vsctl add-port br0 eth$i
    done

    # set the controller IP
    ovs-vsctl set-controller br0 tcp:$2:6653
    ovs-vsctl set-fail-mode br0 secure

    ovs-vsctl show >> /local/repository/init-ovs-switch.log
fi
