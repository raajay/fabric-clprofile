"""
A profile for creating a virtual data center topology.
Each switch is implemented in software using OVS.

Instructions:
    Space holder for parameter descriptions
"""

# Imports# {{{
import os
from enum import Enum
import geni.portal as portal
import geni.rspec.pg as rspec


# }}}

# Topology related functions and classes# {{{
def get_rack_id(index):
    return "rack-" + str(index + 1)


def get_host_id(rnum, hnum):
    return "host-" + str(100 * (rnum) + hnum)


def get_host_interface_id(intf_counter):
    return "intf-" + str(intf_counter + 1)


def get_switch_id(switch_num):
    return "switch-" + str(switch_num)


class Rack(object):  # {{{

    def __init__(self, rack_num):
        self.rack_num = rack_num
        self.id = get_rack_id(rack_num)
        self.hosts = []
        return

    def add_host(self, host):
        self.hosts.append(host)


# }}}

class Host(object):  # {{{

    def __init__(self, rack_num, host_num):
        self.rack_num = rack_num
        self.host_num = host_num
        self.id = get_host_id(rack_num, host_num)
        self.interface = None
        return

    def add_interface(self, intf):
        self.interface = intf


# }}}

class Interface(object):  # {{{

    def __init__(self, interface_num):
        self.interface_num = interface_num
        self.id = get_host_interface_id(interface_num)
        return


# }}}

class Link(object):  # {{{

    def __init__(self, intf1, intf2):
        self.id = "link-" + str(intf1.interface_num) + "-" + str(intf2.interface_num)
        self.interface_1 = intf1
        self.interface_2 = intf2
        return


# }}}

class SwitchType(Enum):  # {{{
    TOR = 1
    AGGREGATION = 2
    CORE = 3


# }}}

class Switch(object):  # {{{

    def __init__(self, switch_num, switch_type):
        self.switch_num = switch_num
        self.id = get_switch_id(switch_num)
        self.stype = switch_type
        self.interfaces = []
        return

    def add_interface(self, intf):
        self.interfaces.append(intf)


# }}}

class DataCenterTopologyType(Enum):  # {{{
    FAT_TREE = 1
    NORMAL = 2


# }}}

class TopologyConfig(object):  # {{{

    def __init__(self, ):
        self.num_racks = 3
        self.num_servers_per_rack = 5
        self.topology_type = DataCenterTopologyType.NORMAL
        return


# }}}

class Topology(object):  # {{{

    def factory(config):
        if config.topology_type == DataCenterTopologyType.NORMAL:
            return NormalTopology(config)
        if config.topology_type == DataCenterTopologyType.FAT_TREE:
            return FatTreeTopology(config)
        return

    factory = staticmethod(factory)

    def get_next_interface_num(self):
        self.interface_counter += 1
        return self.interface_counter

    def __init__(self, config):
        self.interface_counter = 0
        self.racks = []  # collection of all racks
        self.hosts = []  # collection of all hosts
        self.interfaces = []  # collection of host and switch interfaces
        self.switches = []  # list of all switches
        self.links = []  # pair of interfaces

        for r in xrange(config.num_racks):
            rack = Rack(r + 1)
            for h in xrange(config.num_servers_per_rack):
                host = Host(r + 1, h + 1)
                interface = Interface(self.get_next_interface_num())
                host.add_interface(interface)
                self.hosts.append(host)
                self.interfaces.append(interface)
                rack.add_host(host)

            self.racks.append(rack)
        return

    def write(self, output_file):
        with open(output_file, "w") as fp:
            for host in self.hosts:
                fp.write("host:%s\n" % host)
            for sw in self.switches:
                fp.write("switch:%s\n" % sw)
            fp.close()
        return


# }}}

class NormalTopology(Topology):  # {{{

    def __init__(self, config):
        super(NormalTopology, self).__init__(config)
        self.tor_switches = []
        self.aggregation_switches = []
        self.core_switches = []
        # create switches based on the topology that we desire to connect the nodes

        # create ToR switches
        for rack in self.racks:
            tor = Switch(rack.rack_num, SwitchType.TOR)
            for host in rack.hosts:
                # create and add interface to the switch to each host
                sw_intf = Interface(self.get_next_interface_num())
                tor.add_interface(sw_intf)
                self.interfaces.append(sw_intf)
                # create a link between the interface on switch and host interface
                link = Link(sw_intf, host.interface)
                self.links.append(link)
            self.switches.append(tor)  # add to the list of switches
            self.tor_switches.append(tor)  # add to the list of ToR

        # create aggregate switches (for now let us go with one Agg connected
        # to all ToRs)

        agg_start_index = len(self.tor_switches) + 1
        agg = Switch(agg_start_index, SwitchType.AGGREGATION)
        self.switches.append(agg)
        self.aggregation_switches.append(agg)

        for tor in self.tor_switches:
            tor_intf = Interface(self.get_next_interface_num())
            tor.add_interface(tor_intf)
            self.interfaces.append(tor_intf)
            agg_intf = Interface(self.get_next_interface_num())
            agg.add_interface(agg_intf)
            self.interfaces.append(agg_intf)
            link = Link(tor_intf, agg_intf)
            self.links.append(link)
        return


# }}}

class FatTreeTopology(Topology):  # {{{

    def __init__(self, config):
        super(FatTreeTopology, self).__init__(config)
        print "Fat Tree Topology constructor"
        return


# }}}
# }}}

class AddressManager(object):  # {{{

    def __init__(self, topology):
        self.counter = 0
        self.counter2 = 1
        self.topology = topology

    def get_ipv4_addr(self, interface_id=None):
        self.counter += 1
        return "10.10.1.%d" % self.counter

    def get_netmask(self, ):
        return "255.255.255.0"

    def get_ctl_ipv4_addr(self, ):
        return "10.11.1.1"

    def get_mgmt_ipv4_addr(self, ):
        self.counter2 += 1
        return "10.11.1.%d" % self.counter2


# }}}

class RSpecGenerator(object):  # {{{

    def __init__(self, config):  # {{{
        self.component_map = {}
        return  # }}}

    def add_host(self, host, request):  # {{{
        node = request.XenVM(host.id)
        node.cores = 4  # 4 is the max that cloudlab allows us to set
        node.ram = 16 * 1024
        node.disk_image = "urn:publicid:IDN+emulab.net+image+emulab-ops:UBUNTU16-64-STD"

        iface = node.addInterface(host.interface.id)
        iface.component_id = "eth1"
        iface.addAddress(
            rspec.IPv4Address(
                addr_mgr.get_ipv4_addr(),
                addr_mgr.get_netmask()
            )
        )
        self.component_map[host.interface.id] = iface
        self.component_map[host.id] = host
        return  # }}}

    def add_switch(self, sw, request):  # {{{
        node = request.XenVM(sw.id)
        node.cores = 4
        node.ram = 16 * 1024
        node.icon = "https://portal.geni.net/images/router.svg"
        node.disk_image = "urn:publicid:IDN+emulab.net+image+emulab-ops:UBUNTU14-OVS2.31"
        node.addService(
            rspec.Install(
                url="http://www.gpolab.bbn.com/experiment-support/OpenFlowOVS/wireshark.tar.gz",
                path="/local"
            )
        )
        node.addService(
            rspec.Execute(
                shell="sh",
                command="sudo /local/install-script-wire-shark.sh"
            )
        )
        # node.addService(
        #     rspec.Execute(
        #         shell="sh",
        #         command="sudo %s %d %s" % (
        #             os.path.join(SCRIPTS_FOLDER, "init-ovs-switch.sh"),
        #             # provide parameters to the script
        #             len(sw.interfaces) + 1,
        #             addr_mgr.get_ctl_ipv4_addr()
        #         )
        #     )
        # )

        self.component_map[sw.id] = node

        # add management interface eth1
        # NOTES: CloudLab does to respect the component id set in an interface. It just decides the interface in the
        # incrementing order in which they are specified in the rspec
        mgmt_iface = node.addInterface(self.get_mgmt_intf_id(sw.switch_num))
        mgmt_iface.component_id = "eth1"
        mgmt_iface.addAddress(
            rspec.IPv4Address(
                addr_mgr.get_mgmt_ipv4_addr(),
                addr_mgr.get_netmask()
            )
        )
        self.component_map[self.get_mgmt_intf_id(sw.switch_num)] = mgmt_iface

        counter = 2  # etho0 and eth1 are management interfaces
        for intf in sw.interfaces:
            iface = node.addInterface(intf.id)
            iface.component_id = "eth" + str(counter)
            iface.addAddress(
                rspec.IPv4Address(
                    addr_mgr.get_ipv4_addr(),
                    addr_mgr.get_netmask()
                )
            )
            self.component_map[intf.id] = iface
            counter += 1
        return  # }}}

    def add_link(self, link, request):  # {{{
        lnode = request.LAN(link.id)
        lnode.addInterface(self.component_map[link.interface_1.id])
        lnode.addInterface(self.component_map[link.interface_2.id])
        return  # }}}

    def add_controller(self, request):  # {{{
        node = request.XenVM(self.get_controller_id())
        node.cores = 4
        node.ram = 16 * 1024
        node.disk_image = "urn:publicid:IDN+emulab.net+image+emulab-ops:UBUNTU16-64-STD"

        node.addService(
            rspec.Execute(
                shell="sh",
                command="sudo %s" % (
                    os.path.join(SCRIPTS_FOLDER, "init-floodlight-controller.sh"),
                )
            )
        )

        iface = node.addInterface(self.get_controller_intf_id())
        iface.component_id = "eth1"
        iface.addAddress(
            rspec.IPv4Address(
                addr_mgr.get_ctl_ipv4_addr(),
                addr_mgr.get_netmask()
            )
        )
        self.component_map[self.get_controller_id()] = node
        self.component_map[self.get_controller_intf_id()] = iface
        return  # }}}

    def add_mgmt_lan(self, switches, request):  # {{{
        lnode = request.LAN("mgmt-lan")
        lnode.addInterface(self.component_map[self.get_controller_intf_id()])
        for sw in switches:
            lnode.addInterface(self.component_map[self.get_mgmt_intf_id(sw.switch_num)])
        return  # }}}

    @staticmethod
    def get_mgmt_intf_id(switch_num):  # {{{
        return "mgmt-interface-" + str(switch_num)  # }}}

    @staticmethod
    def get_controller_id():
        return "controller"

    @staticmethod
    def get_controller_intf_id():
        return "controller-intf"


# }}}

class RSpecGeneratorConfig(object):  # {{{

    def __init__(self, ):
        return


# }}}

# the main script starts
SCRIPTS_FOLDER = os.path.join("/local", "repository", "scripts")

config = TopologyConfig()
topology = Topology.factory(config)
addr_mgr = AddressManager(topology)
rspec_gen = RSpecGenerator(RSpecGeneratorConfig())

request = portal.context.makeRequestRSpec()

for host in topology.hosts:
    rspec_gen.add_host(host, request)

for sw in topology.switches:
    rspec_gen.add_switch(sw, request)

for link in topology.links:
    rspec_gen.add_link(link, request)

rspec_gen.add_controller(request)
rspec_gen.add_mgmt_lan(topology.switches, request)

portal.context.printRequestRSpec()
